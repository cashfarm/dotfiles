package main

import (
  "log"
  "fmt"
  "os"
  "os/exec"
  "github.com/codegangsta/cli"
)

var tasks = []string{"save"}
var home = os.Getenv("HOUSEKEEPER_PATH")
var apm string = ""

func save(c *cli.Context) {

  pkgFile, err := os.Create(c.Args().First())
  cmd := exec.Command(apm, "list", "--installed", "--bare")
  cmd.Stdout = pkgFile

  if err := cmd.Run(); err != nil {
    log.Fatal(err)
  }
}

func main() {
  err := exec.LookPath("apm")

  if err != nil {
    log.Fatal(`
      The Atom plugin requires apm to be installed.
      Check: https://github.com/atom/apm#installing`)
  }

  app := cli.NewApp()
  app.EnableBashCompletion = true
  app.Name = "HK Atom plugin"
  app.Usage = "Saves and restores installed atom packages"

  app.Commands = []cli.Command{
    {
      Name:  "save",
      Aliases: []string{"c"},
      Usage: "Saves a package.txt file to link/atom/packages.txt",
      Action: save,
      BashComplete: func(c *cli.Context) {
        // This will complete if no args are passed
        if len(c.Args()) > 0 {
          return
        }
        for _, t := range tasks {
          fmt.Println(t)
        }
      },
    },
  }

  app.Run(os.Args)
}
