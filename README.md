The Blacksmith dotfiles
==============================

My home directory config. Using [dotbot](https://github.com/anishathalye/dotbot), [prezto](https://github.com/sorin-ionescu/prezto) and [nanorc](https://github.com/scopatz/nanorc).

Installation
------------------------------

1. Install Homebrew

        $ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

2. Install dependencies

        $ brew install git openssl

3. Clone the repository

        $ git clone https://github.com/svallory/dotfiles.git ~/.dotfiles

4. `cd` into your repo. Run the next commands from there

        $ cd ~/.dotfiles


5. Run the `./unlock` script to decrypt files in your vault or to prepare
your clone for auto encryption.

        $ ./unlock

6. Run the install script

        $ ./install

    or

        $ ./install.linux
