#
# Get the current OS
#
# Returns: a string (one of: mac, linux, freebsd)
#

function is-inited {
  platform='unknown'
  unamestr=`uname`

  if (( "$unamestr" == 'Linux' )); then
     return 'linux'
  elif (( "$unamestr" == 'FreeBSD' )); then
     return 'freebsd'
  elif (( "$unamestr" == 'FreeBSD' )); then
    return 'mac'
  fi

  return 'unknown'
}
