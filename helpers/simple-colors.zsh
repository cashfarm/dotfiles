#!/usr/bin/env zsh

autoload colors; colors

if [[ "$terminfo[colors]" -gt 8 ]]; then
    colors
fi
# The variables are wrapped in \%\{\%\}. This should be the case for every
# variable that does not contain space.
for COLOR in RED GREEN YELLOW BLUE MAGENTA CYAN BLACK WHITE; do
  eval $COLOR='$fg_no_bold[${(L)COLOR}]'
  eval BOLD_$COLOR='$fg_bold[${(L)COLOR}]'
done

RESET=$reset_color

if (( ${@[(i)--export]} <= ${#@} )); then
  export RED GREEN YELLOW BLUE WHITE BLACK
  export BOLD_RED BOLD_GREEN BOLD_YELLOW BOLD_BLUE
  export BOLD_WHITE BOLD_BLACK
fi
