#!/usr/bin/env zsh

source $YAHMDIR/helpers/simple-colors.zsh

autoload yahmlog is-inited mark-inited

BEAN=prezto

if (is-inited $BEAN); then
  yahmlog "Exiting" "This bean was already initialized"
  exit 0
fi

# The absolute, canonical ( no ".." ) path to this script
script=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)/$(basename -- "$0")")

dir=$(dirname $script)

yahmlog
yahmlog ".dotfiles root:" "$(dirname $dir)"
yahmlog "prezto root:" "$(dirname $dir)/home/zprezto/\n"

for f in $dir/**/*.link(.); do
  print "${CYAN}Linking${RESET} home/zprezto.link/${${f#$dir/}%.link} ${CYAN}to${RESET} ${f#$(dirname $dir)/}"
#  ln -Fs $f $(dirname $dir)/home/zprezto.link/${${f#$dir/}%.link}
done

for f in $YAHMDIR/**/*.source(.); do
  echo "${RED}sourcing${RESET} $f"
done

mark-inited $BEAN
